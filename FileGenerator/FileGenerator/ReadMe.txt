﻿Narzędzie wykonane w ramach [OPC-636].

# Opis #
Tryb generowania (bez -mode): generuje foldery (z możliwością konfigurowalnego zagnieżdżania) a następnie wypełnia je plikami.
Tryb przyrostu (z -mode): modyfikuje pliki dopisując do nich w losowych miejscach konfigurowalną ilość danych.
Nie jest możliwa praca w wielu trybach na raz.

# Tryby pracy #
* Generowanie (domyślny)	generuje pliki i foldery
* Przyrost					zmienia wygenerowane pliki

# Przełączniki #
Kolejność podawania przełączników, wielkość użytych liter oraz ilość/kolejność użytych dopuszczonych znaków 
poprzedzających przełącznik ('-', '/') nie mają znaczenia.
	Przełącznik		Typ			Opis [opcje]
	-help			[null]		Wyświetlenie wszystkich możliwych parametrów
	-s				[null]		Tryb cichy (brak komunikatów - użycie przyspiesza generowanie o 17%)
	-v				[null]		Tryb debug (wyświetlanie dodatkowych informacji - pominięcie przyspiesza generowanie o 10%)
	-i				[null]		Tryb interaktywnego generowania (wprowadzanie ręczne) [disabled: too much work]
	
	Wymagane (zawsze)
	-p				[string]	Ścieżka do folderu roboczego
	-min			[Int32]		Minimalna wielkość pliku/zmiany
	-max			[Int32]		Maksymalna wielkość pliku/zmiany
	-u				[string]	Jednostka rozmiaru pliku/zmiany [b|kb|mb|gb]

	Opcjonalne
	-text			[null]		Przełącza tryb pracy na plikach na tekstowy [default: false, pliki binarne]
	-rp				[string]	Ścieżka do wygenerowanego raportu
	-noreport		[null]		Wyłącza generowanie raportu

Tryb generowania	
	Wymagane
	-dc				[Int32]		Ilość folderów
	-c				[Int32]		Ilość plików
	Opcjonalne
	-maxdnl			[Int32]		Maksymalna długość nazwy folderu [default: 10]
	-mindnl			[Int32]		Minimalna długość nazwy folderu [default: 1]
	-dnl			[Int32]		Długość nazwy folderu [default: => 1-10] nadpisuje -mindnl i -max dnl
	-maxfnl			[Int32]		Maksymalna długość nazwy pliku [default: 50]
	-minfnl			[Int32]		Minimalna długość nazwy pliku [default: 1]
	-fnl			[Int32]		Długość nazwy pliku [default: => 1-50] nadpisuje -minfnl i -maxfnl
	-fel			[Int32]		Długość rozszerzenia pliku [default: => 1-48] nadpisuje -minfnl, -maxfnl i -fnl
	-ext			[Int32]		Rozszerzenie plików [default: random] nadpisuje -minfnl, -maxfnl, -fnl i -ext
	-exts			[strings]	Dopuszczalne rozszerzenia plików rozdzielone ',' [np. -exts bin,exe,docx,pdf,txt]
	-roff			[null]		Wyłączenie generowania plików z losowymi nazwami (nazwy od 0 do Int32.MaxValue) [default: no]
	-dd				[Int32]		Maksymalna głębokość zagnieżdzenia folderu [default: 10]
	-dp				[Int32]		Prawdopodobieństwo zagnieżdzenia folderu w innym [default: 50]
	-chars			[string]	Redefiniuje znaki użyte w nazwach plików 
								[warning: ograniczenie zasobu do kilku znaków doprowadzi do awarii - wyszukiwanie losowej nazwy folderu zajmie zbyt długo/będzie niemożliwe]
	-extchars		[string]	Redefiniuje znaki użyte w rozszerzeniach plików
Tryb przyrostu
	Wymagane
	-mode			[null]		Sygnalizuje pracę w trybie przyrostu
	Opcjonalne
	-c				[Int32]		Definiuje ile plików powinno zostać dotkniętych zmianą (wyklucza -fp) [default: all]
	-fp				[Int32]		Procent zmienianych plików [default: all]
	-imin			[Int32]		Minimalna ilość zmian w pliku [default: 1]
	-imax			[Int32]		Maksymalna ilość zmian w pliku [default: 1]
	-bs				[Int32]		Wielkość bufora [default: 1024 * 1024 * 8 = 8MB]
	-norec			[null]		Wyłącza modyfikowanie plików w podkatalogach

# Zapisane ustawienia #
W celu przyspieszenia pracy możliwe jest używanie zapisanych ustawień. W tym celu podczas wywoływania programu należy podać ścieżkę do pliku 
ustawień. Plik ustawień powinien zawierać parametry pracy, po jednym na jedną linię (czytane góra->dół).
	
# Przykładowe wywołania #
FileGenerator.exe -p C:\tests\OPC-636 -c 50 -dc 10 -min 100 -max 1000 -u kb 
	wygeneruje 50 plików i 10 katalogów o minimalnej wielkości pliku 100KB a maksymalnej 1000KB do katalogu 
	C:\tests\OPC-636\17-06-16\1 używając wartości domyślnych długości nazwy pliku (50 znaków) i katalogu (10 znaków).
FileGenerator.exe -p C:\tests\OPC-636 -min 1 -max 100 -u b -mode
	doda do każdego pliku w katalogu i podkatalogach C:\tests\OPC-636 od 1 do 100B losowych danych

FileGenerator.exe -p C:\tests\OPC-636 -c 100 -dc 10 -min 10 -max 100 -u kb -dd 5 -dp 100 -chars qwertyuiop -extchars test
	wygeneruje w katalogu C:\tests\OPC-636 100 plików o minimalnej wielkości pliku 10KB a maksymalnej 100KB o nazwach 
	składających się ze znaków qwertyuiop i rozszerzeniach składających się ze znaków tes i 10 katalogów o maksymalnej głębokości zagnieżdżenia 
	5 i prawdopodobieństwie zagłębienia 100%

FileGenerator.exe c 10 -dc 10 --mIn 10 -max 100 -u mB p C:\tests\OPC-636 dd 10 -------dp 100 roff v -fnl 6 -dfn 7
	wygeneruje - mimo różnicy wielkości liter i ilości użytych znaków '-' - w katalogu C:\tests\OPC-636 pożądane 10 plików 
	w 10 folderach (każdy folder zostanie umieszczony w poprzednim wygenerowanym ponieważ prawdopodobieństwo zagnieżdżenia 
	zostało ustawione na 100%). Pliki będą miały nazwy o długości 6 znaków zaczynające się od 000000 zaś katalogi nazwy 
	o długości 7 znaków zaczynające się od 0000000.
FileGenerator.exe -c 50 -dc 10 -min 100 -max -u kb -p C:\tests\OPC-636
	Wyświetli komunikat "Size of file to generate must be integer." i zakończy się błędem (exit code -1).

# Klasy #
	Klasa			Opis
	Properties		Przechowuje ustawienia programu
	FileManager		Zarządza plikami
	FolderManager	Zarządza folderami
	RandomGenerator	Nadrzędna klasa menedżerów - zawiera logikę generowania losowych nazw
# Metody #
Pełna dokumentacja w kodzie.

# Bugs #
FileStream will not open Win32 devices such as disk partitions and tape drives. Avoid use of "\\.\" in the path.
	Nie udało się odtworzyć błędu - wygenerowanie pliku zawierającego w nazwie "\\.\\" (nie powinno być możliwe)?

//old:
>> Klasy :
	Nazwa klasy					Opis
	FileGenerator				Klasa przechowująca metody tworzenia plików z określoną nazwą oraz wielkością

	FolderGenerator				Klasa przechowująca metody tworzenia folderów w folderach do n zagnieżdżenia z losową nazwą.

	RandomEngine (S)			Klasa przechowująca metody generowania losowych nazw ze wszystkimi znakami specjalnymi dozwolonymi 
								do tworzenia folderów oraz generowania losowych znaków w plikach.

	Properties				Klasa przechowująca wszystkie informacje globalne.
	
>> Metody :

FileGeneratorClass

	Nazwa metody							Opis
	void CreateFile()						Tworzy plik w podanej ścieżce path o rozmiarze z przedziału (min max).

	private void FileCreator(				Metoda do tworzenia pliku.
	string fullPath, int tempSize, 
	PropertyClass.UnitSize DataSize)

	private string getFileName()			Pobiera parametry z obiektu klasy PropertyClass i na tej podstawie generuje nazwy plików.
							


FolderGenerator

	* String[] FolderName {get, private set} - przechowuje nazwy wygenerowanych folderów.

	Nazwa metody							Opis
	void CreateFolder (string path)			Tworzy folder w podanej ścieżce path.

	void CreateFolder (string path,			Tworzy count folderów w podanej ścieżce path.
	int count)	

	void CreateFolder (string path,			Tworzy count folderów o długości nazwy nameLength w podanej ścieżce path. 
	int count, int nameLength)	
	

RandomGenerator

	Nazwa metody							Opis
	string RandomFileName()					Zwraca losową nazwę pliku o losowej długości składającą się ze wszystkich
											dozwolonych znaków w Windowsie.

	string RandomFileName(int count)		Zwraca losową nazwę pliku o podanej długości count składającą się ze wszystkich 
											dozwolonych znaków w Windowsie.

	string RandomFileName( int count,		Zwraca losową nazwę pliku o podanej długości count składającą się ze wszystkich  
	string extension)						dozwolonych znaków w Windowsie o rozszerzeniu extension.

	string RandomFolderName ()				Zwraca losową nazwę folderu o losowej długości składającą się ze wszystkich 
											dozwolonych znaków w Windowsie.

	string RandomFolderName (int count)		Zwraca losową nazwę folderu o długości count składającą się ze wszystkich 
											dozwolonych znaków w Windowsie.
	