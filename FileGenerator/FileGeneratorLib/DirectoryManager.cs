﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FileGeneratorLib
{
    /// <summary>
    /// Used to create folders.
    /// </summary>
    public class DirectoryManager : RandomGenerator
    {
        public List<string> Folders { get; private set; } = new List<string>();

        /// <summary>
        /// Initialize DirectoryManager with program Properties and Random.
        /// Created TestTokenFile.
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="random"></param>
        public DirectoryManager(Properties properties, Random random) : base(properties, random)
        {
            Directory.CreateDirectory(properties.FolderPath);
            File.Create($"{properties.FolderPath}{Properties.TokenFileName}");
        }

        public void Generate()
        {
            if (properties.DirectoryCount > 0)
            {
                if (!properties.Silent && properties.Verbose) Console.WriteLine($"Creating {properties.DirectoryCount} directories:");
                int i = 0;
                while (i < properties.DirectoryCount)
                {
                    CreateFolder(properties.FolderPath, 0, ref i);
                }
                if (!properties.Silent && properties.Verbose) Console.WriteLine();
            }
        }

        public void CreateFolder(string path, int currentDepth, ref int i)
        {
            currentDepth++;
            string folderPath = Path.Combine(path, GenerateRandomString(path));
            if (!Directory.Exists(folderPath) && i < properties.DirectoryCount)
            {
                i++;
                Directory.CreateDirectory(folderPath);
                Folders.Add(folderPath);
                if (!properties.Silent && properties.Verbose)
                {
                    if (properties.DirectoryCount < 500)
                    {
                        Console.Write(".");
                    }
                    else
                    {
                        if ((i + 1) % 1000 == 0)
                        {
                            Console.Write("M");
                        }
                        else if ((i + 1) % 100 == 0)
                        {
                            Console.Write("C");
                        }
                        else if ((i + 1) % 10 == 0)
                        {
                            Console.Write(".");
                        }
                    }
                }
            }
            if (currentDepth < properties.MaxDirectoryDepth
                && random.Next(100) < properties.DirectoryDepthPropability
                && i < properties.DirectoryCount)
            {
                CreateFolder(folderPath, currentDepth, ref i);
            }
        }
    }
}
