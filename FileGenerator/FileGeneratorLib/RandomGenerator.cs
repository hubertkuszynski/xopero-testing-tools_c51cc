﻿using System;
using System.Text;

namespace FileGeneratorLib
{
    public abstract class RandomGenerator
    {
        protected Random random;
        protected Properties properties;
        private long fileNameNumber = 0;
        public RandomGenerator(Properties properties, Random random)
        {
            this.properties = properties;
            this.random = random;
        }

        public string GenerateRandomString(string path, bool isFile = false)
        {
            int allowedLength;
            int maxLength;
            int minLength;
            string name;
            if (isFile)
            {
                allowedLength = Properties.MaxLength - path.Length - properties.MaxFileNameLength;
                maxLength = properties.MaxFileNameLength;
                minLength = properties.MinFileNameLength;
                if (minLength < allowedLength)
                {
                    name = maxLength < allowedLength
                        ? name = GenerateName(minLength, maxLength, isFile)
                        : name = GenerateName(minLength, allowedLength, isFile);
                }
                else
                {
                    name = GenerateName(allowedLength, allowedLength, isFile);
                }
            }
            else
            {
                allowedLength = Properties.MaxLength - path.Length - properties.MaxDirectoryNameLength;
                maxLength = properties.MaxDirectoryNameLength;
                minLength = properties.MinDirectoryNameLength;
                if (properties.MinDirectoryNameLength < allowedLength)
                {
                    name = properties.MaxDirectoryNameLength < allowedLength
                        ? name = GenerateName(properties.MinDirectoryNameLength, properties.MaxDirectoryNameLength)
                        : name = GenerateName(properties.MinDirectoryNameLength, allowedLength);
                }
                else
                {
                    name = GenerateName(allowedLength, allowedLength);
                }
            }
            return name;
        }

        private string GenerateName(int minLength, int maxLength, bool isFile = false)
        {
            string name = string.Empty;
            int min;
            int max;
            if (properties.RandomNameMode)
            {
                if (isFile) //it is a file
                {
                    if (properties.Ext != string.Empty)
                    {
                        int reserved = properties.Ext.Length + 1;
                        min = minLength - reserved > 0
                            ? minLength - reserved
                            : 0;
                        max = maxLength - reserved > 0
                            ? maxLength - reserved
                            : 0;
                        name = $"{GenerateRandomName(min, max)}.{properties.Ext}";
                    }
                    else if (properties.GetExtsCount() > 0)
                    {
                        string ext = properties.GetExts()[random.Next(properties.GetExts().Length)];
                        min = properties.MinFileNameLength > maxLength - ext.Length
                            ? maxLength - ext.Length - 1
                            : properties.MinFileNameLength;
                        name = $"{GenerateRandomName(min, maxLength - ext.Length - 1)}.{ext}";
                    }
                    else if (properties.FileExtensionLength > 0)
                    {
                        name = $"{GenerateRandomName(properties.MinFileNameLength, maxLength - properties.FileExtensionLength)}.{GenerateRandomName(1, properties.FileExtensionLength, true)}";
                    }
                    else
                    {
                        int extLength = random.Next(maxLength - 1); // Random.Next(n) returns number from 0 to n-1
                        if (extLength == 0)
                        {
                            name = $"{GenerateRandomName(properties.MinFileNameLength, properties.MaxFileNameLength)}";
                        }
                        else
                        {
                            min = properties.MinFileNameLength - extLength - 1 > 1 
                                ? properties.MinFileNameLength - extLength - 1
                                : 1; // we don't want nasty negative values
                            max = properties.MaxFileNameLength - extLength - 1 > 1
                                ? properties.MaxFileNameLength - extLength - 1
                                : 1; // we don't want nasty negative values
                            name = $"{GenerateRandomName(min, max)}{GenerateRandomName(0, extLength, true)}";
                        }
                    }

                }
                else //it is a folder
                {
                    name = GenerateRandomName(minLength, maxLength);
                }
            }
            else
            {
                int correctedLength = maxLength - fileNameNumber.ToString().Length;
                if (isFile && properties.Ext != string.Empty)
                {
                    correctedLength -= properties.Ext.ToString().Length + 1;
                    name = $".{properties.Ext}";
                }
                name = $"{fileNameNumber.ToString()}{name}";
                for (int i = 0; i < correctedLength; i++)
                {
                    name = $"0{name}";
                }
                fileNameNumber++;
            }
            return name;
        }

        /// <summary>
        /// Generates random chars.
        /// </summary>
        /// <param name="min">Minimum number of characters to pick</param>
        /// <param name="max">Maximum number of characters to pick</param>
        /// <param name="ext">States if generated string is extension</param>
        /// <returns>
        /// if(ext) returns max chars from properties.ExtensionChars prefixed by '.'
        /// else returns from min to max from properties.Chars
        /// </returns>
        internal string GenerateRandomName(int min, int max, bool ext = false)
        {
            int pick = ext ? max : random.Next(min, max + 1);
            StringBuilder sb = new StringBuilder(pick + 1);
            if (ext && max != 0) { sb.Append("."); }
            for (int i = 0; i < pick; i++)
            {
                if (ext)
                {
                    sb.Append(properties.ExtensionChars[random.Next(0, properties.ExtensionChars.Length)]);
                }
                else
                {
                    sb.Append(properties.Chars[random.Next(0, properties.Chars.Length)]);
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Metoda zwracająca tablice losowych bajtów o określonej wielkości.
        /// </summary>
        public byte[] GenerateRandomBytes(int count)
        {
            byte[] randomByte = new byte[count];
            random.NextBytes(randomByte);
            return randomByte;
        }
    }
}
