﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Threading;
using XoperoTestingTools.Data;

namespace XoperoTestingTools.Unit
{
    [TestClass]
    public class TimeParameterValidatorTests
    {
        [TestMethod]
        public void MidnightHourValidationReturnsChangedMidnightTime()
        {
            DateTime mockMidnightTime = TimeParameterValidator.midnightTime;
            TimeParameterValidator.TimeParameterMidnightHourValidator();
            Assert.AreNotEqual(mockMidnightTime, TimeParameterValidator.midnightTime);
        }
        [TestMethod]
        public void CustomHourValidatorReturnsChangedDateTime()
        {
            DateTimeParameters mockInputTime = new DateTimeParameters()
            {
                Year = 2000,
                Month = 6,
                Day = 6,
                Hour = 11,
                Minute = 11
            };
            var mockOutputTime = TimeParameterValidator.TimeParameterCustomHourValidator(mockInputTime);
            Assert.AreNotEqual(mockInputTime, mockOutputTime);
        }
        [TestMethod]
        public void CascadeInputValidatorThrowsArgumentOutOfRangeExceptionDay()
        {
            DateTimeParameters mockInputTime = new DateTimeParameters()
            {
                Year = 2000,
                Month = 6,
                Day = 31,
                Hour = 11,
                Minute = 11
            };
            Dispatcher.CurrentDispatcher.BeginInvoke(() => 
            {
                MainWindow window = new MainWindow();
                Assert.ThrowsException<ArgumentOutOfRangeException>(() => TimeParameterValidator.CascadeInputValidator(mockInputTime));
            });
        }
    }
}
