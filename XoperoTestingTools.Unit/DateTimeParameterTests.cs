﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XoperoTestingTools.Data;

namespace XoperoTestingTools.Unit
{
    [TestClass]
    public class DateTimeParameterTests
    {
        [TestMethod]
        public void DateTimeParameterInitialize()
        {
            DateTimeParameters dateTime = new DateTimeParameters();
            Assert.IsNotNull(dateTime.Year);
        }
        [TestMethod]
        public void DateTimeParameterInitializeCustomParameters()
        {
            DateTimeParameters dateTime = new DateTimeParameters()
            {
                Year = 2000,
                Month = 6,
                Day = 11,
                Hour = 11,
                Minute = 11
            };
            Assert.AreEqual(2000, dateTime.Year);
            Assert.AreEqual(6, dateTime.Month);
            Assert.AreEqual(11, dateTime.Day);
            Assert.AreEqual(11, dateTime.Hour);
            Assert.AreEqual(11, dateTime.Minute);
        }
    }
}
