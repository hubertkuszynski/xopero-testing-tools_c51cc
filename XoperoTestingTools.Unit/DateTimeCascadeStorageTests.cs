﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XoperoTestingTools.Data;

namespace XoperoTestingTools.Unit
{
    [TestClass]
    public class DateTimeCascadeStorageTests
    {
        [TestMethod]
        public void InitializeDateTimeCascadeStorage()
        {
            DateTimeCascadeStorage cascadeStorage = new DateTimeCascadeStorage();
            Assert.IsNotNull(cascadeStorage);
        }
        [TestMethod]
        public void CascadeIntervalStorageReturnsValue()
        {
            DateTimeCascadeStorage cascadeStorage = new DateTimeCascadeStorage();
            cascadeStorage.CascadeInterval = 5;
            Assert.AreEqual(5, cascadeStorage.CascadeInterval);
        }
        [TestMethod]
        public void CascadeIntervalStorageAddStructToList()
        {
            DateTimeCascadeStorage cascadeStorage = new DateTimeCascadeStorage();
            cascadeStorage.AddToList(new DateTimeParameters());
            Assert.IsTrue(cascadeStorage._List.Count == 1);
        }
    }
}
