﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Controls;
using System.Windows.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XoperoTestingTools.View;

namespace XoperoTestingTools.Unit
{
    [TestClass]
    public class HelpWindowTests
    {
        [TestMethod]
        public void HelpWindowInitialize()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(() => 
            {
                HelpWindow window = new HelpWindow();
                Assert.IsNotNull(window);
            });
        }
    }
}
