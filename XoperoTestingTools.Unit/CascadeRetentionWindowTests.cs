﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XoperoTestingTools.Data;
using XoperoTestingTools.View;

namespace XoperoTestingTools.Unit
{
    [TestClass]
    public class CascadeRetentionWindowTests
    {
        [TestMethod]
        public void InitializeCascadeRetentionWindow()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(() =>
            {
                AddCascadeRetentionWindow window = new AddCascadeRetentionWindow();
                Assert.IsTrue(window.IsInitialized);
            }
            );
        }
    }
}
