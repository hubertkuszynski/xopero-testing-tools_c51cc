using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using XoperoTestingTools.Data;

namespace XoperoTestingTools.Unit
{
    [TestClass]
    public class MainWindowTests
    {
        [TestMethod]
        public void InitializeObject()
        {
            MainWindow mainWindow = new MainWindow(); 
            //mainWindow.InitializeComponent();
            Assert.IsNotNull(mainWindow);
        }
        [TestMethod]
        public void InitializeMainImage()
        {
            Image newImage = new Image();
            newImage.Source = new BitmapImage(new Uri(Environment.CurrentDirectory.Replace("\\bin\\Debug\\netcoreapp3.1", "\\Image\\xopero.png")));
            Assert.AreEqual(Environment.CurrentDirectory.Replace("\\bin\\Debug\\netcoreapp3.1", "\\Image\\xopero.png"), newImage.Source);
        }
        [TestMethod]
        public void TimeScheduleApply()
        {

        }
        [TestMethod]
        public void MainWindowCascadeRetentionListValidate()
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.CascadeStorage.AddToList(new DateTimeParameters());
            mainWindow.ValidateCascadeRetention();
            Assert.IsTrue(mainWindow.CascadeStorage._List.Count == 0);
        }
    }
}
