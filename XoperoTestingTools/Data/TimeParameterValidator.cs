﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace XoperoTestingTools.Data
{
    public static class TimeParameterValidator
    {
        public static DateTime midnightTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
        public static void TimeParameterMidnightHourValidator()
        {
            if (DateTime.Now.Hour >= midnightTime.Hour && DateTime.Now.Day == midnightTime.Day)
            {
                int lastMonthInYear = 12;
                int lastDayInMonth = DateTime.DaysInMonth(midnightTime.Year, midnightTime.Month);

                if (lastMonthInYear == DateTime.Now.Month && lastDayInMonth == DateTime.Now.Day)
                {
                    midnightTime = new DateTime(DateTime.Now.Year + 1, 1, 1, 0, 0, 0);
                }
                else if (lastDayInMonth == DateTime.Now.Day)
                {
                    midnightTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month + 1, 1, 0, 0, 0);
                }
                else
                {
                    midnightTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1, 0, 0, 0);
                }
            }
        }
        public static DateTimeParameters TimeParameterCustomHourValidator(DateTimeParameters inputTime)
        {
            int lastMonthInYear = 12;
            int lastDayInMonth = DateTime.DaysInMonth(inputTime.Year, inputTime.Month);

            if (lastMonthInYear == DateTime.Now.Month && lastDayInMonth == DateTime.Now.Day)
            {
                inputTime = new DateTimeParameters()
                {
                    Year = ++inputTime.Year,
                    Month = 1,
                    Day = 1,
                    Hour = inputTime.Hour,
                    Minute = inputTime.Minute,
                    Second = inputTime.Second
                };
            }
            else if (lastDayInMonth == DateTime.Now.Day)
            {
                inputTime = new DateTimeParameters()
                {
                    Year = inputTime.Year,
                    Month = ++inputTime.Month,
                    Day = 1,
                    Hour = inputTime.Hour,
                    Minute = inputTime.Minute,
                    Second = inputTime.Second
                };
            }
            else
            {
                inputTime = new DateTimeParameters()
                {
                    Year = inputTime.Year,
                    Month = inputTime.Month,
                    Day = ++inputTime.Day,
                    Hour = inputTime.Hour,
                    Minute = inputTime.Minute,
                    Second = inputTime.Second
                };
            }

            return inputTime;
        }
        public static void CascadeInputValidator(DateTimeParameters dateTime)
        {
            int lastMonthInYear = 12;
            int lastDayInMonth = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);

            if (dateTime.Month <= 0 || dateTime.Month > lastMonthInYear)
            {
                ((MainWindow)Application.Current.MainWindow).ValidateCascadeRetention();
                throw new ArgumentOutOfRangeException("Błąd - miesiąc ma niepoprawną wartość");
            }
            if (dateTime.Year < 1970 || dateTime.Year > 2200)
            {
                ((MainWindow)Application.Current.MainWindow).ValidateCascadeRetention();
                throw new ArgumentOutOfRangeException("Błąd - rok ma niepoprawną wartość");
            }
            if (dateTime.Day <= 0 || dateTime.Day > lastDayInMonth)
            {
                ((MainWindow)Application.Current.MainWindow).ValidateCascadeRetention();
                throw new ArgumentOutOfRangeException("Błąd - dzień ma niepoprawną wartość");
            }
            if (dateTime.Hour <0 || dateTime.Hour > 23)
            {
                ((MainWindow)Application.Current.MainWindow).ValidateCascadeRetention();
                throw new ArgumentOutOfRangeException("Błąd - Godzina ma niepoprawną wartość");
            }
            if (dateTime.Minute <0 || dateTime.Minute > 59)
            {
                ((MainWindow)Application.Current.MainWindow).ValidateCascadeRetention();
                throw new ArgumentOutOfRangeException("Błąd - Minuta ma niepoprawną wartość");
            }
        }
    }
}
