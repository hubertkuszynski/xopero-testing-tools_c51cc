﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace XoperoTestingTools.Data
{
    public struct DateTimeParameters
    {
        public short Year { get; set; }
        public short Month { get; set; }
        public short DayOfWeek { get; set; }
        public short Day { get; set; }
        public short Hour { get; set; }
        public short Minute { get; set; }
        public short Second { get; set; }
        public short Milliseconds { get; set; }

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetSystemTime(ref DateTimeParameters systemTime);
    }
}
