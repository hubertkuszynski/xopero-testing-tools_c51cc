Xopero Testing Tools to program umożliwiający testy retencji długoterminowej oraz harmonogramu poprzez wywołanie metody SetSystemTime w Kernel32.dll

1. Zmiana daty w celu wywołania harmonogramu

a) zmiana godziny bez zmiany daty
b) wybranie opcji zmiany czasu o 00;00 każdego dnia.
c) wybranie interwału zmiany czasu wedle użytkownika
d) wywoływanie kaskadowe - zmiana czasu skokowo wedle zadeklarowanego interwału.

2. Generator plików z interwałem retencji

Wybieramy ścieżkę docelową dla katalogu - program utworzy katalog i wygeneruje pliki w lokalizacji docelowej.

Określamy rozmiar plików od-do i wybieramy rozmiar pliku.

Określamy ilośc plików do wygenerowania

Opcjonalnie zaznaczamy możliwość generowania plików interwałowo.