﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace XoperoTestingTools.View
{
    /// <summary>
    /// Interaction logic for HelpWindow.xaml
    /// </summary>
    public partial class HelpWindow : Window
    {
        private string _path = Environment.CurrentDirectory.Replace("\\bin\\Debug\\netcoreapp3.1", "\\Data\\ReadMe.txt");
        public HelpWindow()
        {
            InitializeComponent();
            txtBoxHelp.Text = File.ReadAllText(_path);
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
