﻿using System;
using System.Windows;
using XoperoTestingTools.Data;

namespace XoperoTestingTools.View
{
    /// <summary>
    /// Interaction logic for AddCascadeRetentionWindow.xaml
    /// </summary>
    public partial class AddCascadeRetentionWindow : Window
    {
        public AddCascadeRetentionWindow()
        {
            InitializeComponent();
        }
        #region private UIMethods
    #warning todo - enabling object capable of multiple interval changes..
    #warning todo - fix view - delete unnessesary columns & rename them to polish
        /// <summary>
        /// button method used to add DateTimeParams into CascadeRetentionList
        /// </summary>
        /// <param name="sender">object used to invoke</param>
        /// <param name="e">event arguments</param>
        private void buttonAddCascadeRetentionToList_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTimeParameters dateTimeInput = new DateTimeParameters();
                dateTimeInput.Year = short.Parse(txtBoxDateTimeParameterYear.Text);
                dateTimeInput.Month = short.Parse(txtBoxDateTimeParameterMonth.Text);
                dateTimeInput.Day = short.Parse(txtBoxDateTimeParameterDay.Text);
                dateTimeInput.Hour = short.Parse(txtBoxDateTimeParameterHour.Text);
                dateTimeInput.Minute = short.Parse(txtBoxDateTimeParameterMinute.Text);
                dateTimeInput.Second = (short)DateTime.Now.Second;

                if (double.Parse(txtBoxCustomScheduleTime.Text) <= 0)
                {
                    txtBoxCustomScheduleTime.Text = string.Empty;
                    throw new ArgumentOutOfRangeException("Interwał nie może być mniejszy niż zero");
                }
                else
                {
                    ((MainWindow)Application.Current.MainWindow).CascadeStorage.CascadeInterval = double.Parse(txtBoxCustomScheduleTime.Text);
                    txtBoxCustomScheduleTime.IsEnabled = false;
                }

                TimeParameterValidator.CascadeInputValidator(dateTimeInput);
                ((MainWindow)Application.Current.MainWindow).CascadeStorage.AddToList(dateTimeInput);
            }
            catch(ArgumentOutOfRangeException error)
            {
                MessageBox.Show(error.Message, "Błąd");
                validateCascadeInputs();
            }
            catch(FormatException)
            {
                MessageBox.Show("Wprowadzone dane są błędne - wprowadź używając liczb, nie liter - lub uzupełnij puste pola", "Błąd");
                validateCascadeInputs();
            }
            catch (Exception err)
            {
                MessageBox.Show($"Wystąpił inny nieoznakowany błąd {err.ToString()}", "Krytyczny błąd");
                validateCascadeInputs();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">object used</param>
        /// <param name="e">event args</param>
        private void buttonCloseCascadeWindow_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            ((MainWindow)Application.Current.MainWindow).dataGridCascadeRetention.ItemsSource = null;

            if(((MainWindow)Application.Current.MainWindow).CascadeStorage._List.Count > 0)
            {
                ((MainWindow)Application.Current.MainWindow).dataGridCascadeRetention.ItemsSource = ((MainWindow)Application.Current.MainWindow).CascadeStorage._List;
                ((MainWindow)Application.Current.MainWindow).txtBoxScheduleCustomTime.Text = ((MainWindow)Application.Current.MainWindow).CascadeStorage.CascadeInterval.ToString();
            }
        }
        #endregion
        #region private methods
        /// <summary>
        /// Method used to validate inputs for cascade retention system
        /// </summary>
        private void validateCascadeInputs()
        {
            txtBoxCustomScheduleTime.Text = string.Empty;
            txtBoxDateTimeParameterDay.Text = string.Empty;
            txtBoxDateTimeParameterHour.Text = string.Empty;
            txtBoxDateTimeParameterMinute.Text = string.Empty;
            txtBoxDateTimeParameterMonth.Text = string.Empty;
            txtBoxDateTimeParameterYear.Text = string.Empty;
        }
        #endregion
    }
}
