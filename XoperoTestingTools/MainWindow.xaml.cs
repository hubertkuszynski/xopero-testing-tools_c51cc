﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using XoperoTestingTools.Data;
using XoperoTestingTools.View;
using FileGenerator;
using System.Windows.Media.Imaging;
using XoperoTestingTools.Logs;

namespace XoperoTestingTools
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region objects
        /// <summary>
        /// inicjalizacja obiektu zmiany czasu oraz obiekt wywołania kaskadowego.
        /// </summary>
        /// 
        public DateTimeCascadeStorage CascadeStorage;
        public Logger Logger = new Logger();
        private Timer _scheduleIntervalTimer;
        private Timer _generatorIntervalTimer;
        private DateTimeParameters inputDateTime = new DateTimeParameters()
        {
            Day = (short)DateTime.Now.Day,
            Month = (short)DateTime.Now.Month,
            Year = (short)DateTime.Now.Year
        };
        
        #endregion
        #region const fields
        /// <summary>
        /// constant interval time - 1` (minute)
        /// </summary>
        private const double defaultScheduleInterval = 60 * 1000;
        private const int regionDate = -2;
        #endregion
        #region enums
        /// <summary>
        /// Generator Enums for filesize change
        /// </summary>
        public enum FileSizeType
        {
            b,  //   bytes
            kb, //   kilobytes
            mb, //   megabytes
            gb  //   gigabytes
        }
        #endregion
        #region private fields
        private int _concatPath = 1;
        private int _cascadeCounter = 0;
        private string _fileType; // declared in switch case body
        #endregion
        #region constructor
        /// <summary>
        /// initializing UI object (MainWindow)
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            cbxFileSizeType.Items.Add("Bajty -B");
            cbxFileSizeType.Items.Add("Kilobajty -KB");
            cbxFileSizeType.Items.Add("Megabajty -MB");
            cbxFileSizeType.Items.Add("Gigabajty -GB");
            TimeParameterValidator.TimeParameterMidnightHourValidator();
            Logger.DebugFileLog("Application started!");
            txtBoxLogs.Text = Logger.InfoWindowLog("Witaj w Xopero Testing Tools!");
        }
        #endregion
        #region private events
        /// <summary>
        /// events called when schedule and generator interval hits the marked time.
        /// </summary>
        /// <param name="source"> object source</param>
        /// <param name="ea"> Elapsed ea </param>
        private void onTimedScheduleEvent(object source, ElapsedEventArgs ea)
        {
            TimeParameterValidator.TimeParameterMidnightHourValidator();
            inputDateTime = TimeParameterValidator.TimeParameterCustomHourValidator(inputDateTime);
            DateTimeParameters.SetSystemTime(ref inputDateTime);
            txtBoxLogs.Text += Logger.InfoWindowLog($"Wywołanie zmiany czasu o północy - Zmieniono datę i godzinę na {inputDateTime}");
            _scheduleIntervalTimer.Stop();
            var interval = (TimeParameterValidator.midnightTime - DateTime.Now).TotalMilliseconds;
            _scheduleIntervalTimer = new Timer(interval);
            _scheduleIntervalTimer.Start();
        }
        private void onTimedGeneratorEvent(object source, ElapsedEventArgs ea)
        {
            Dispatcher.BeginInvoke(() =>
            {
                string[] args = new string[] { "p", txtBoxFilePath.Text + "\\" + _concatPath.ToString(), "min", txtBoxMinFileSize.Text, "max", txtBoxMaxFileSize.Text, "u", _fileType, "c", txtBoxFileAmount.Text, "dc", "0" };
                Program.Main(args);
                txtBoxLogs.Text += Logger.InfoWindowLog($"Wykonano {_concatPath} pętlę do katalogu C:\\{txtBoxFilePath.Text}\\{_concatPath.ToString()}");
            });
        }
        private void onTimedCustomScheduleEvent(object source, ElapsedEventArgs ea)
        {
            inputDateTime = TimeParameterValidator.TimeParameterCustomHourValidator(inputDateTime);
            DateTimeParameters.SetSystemTime(ref inputDateTime);
            txtBoxLogs.Text += Logger.InfoWindowLog($"Wywołano zmianę godziny i daty na {inputDateTime} (interwał {txtBoxScheduleCustomTime.Text})");
        }
        private void onCascadeScheduleEvent(object source, ElapsedEventArgs ea)
        {
            inputDateTime = CascadeStorage._List[_cascadeCounter++];
            var checkMidnightInputHour = inputDateTime.Hour == 0 ? inputDateTime.Hour = 22 : inputDateTime.Hour += regionDate; // need to refactor this declaration
            DateTimeParameters.SetSystemTime(ref inputDateTime);
            txtBoxLogs.Text += Logger.InfoWindowLog($"Wywołano zmianę godziny i daty na {inputDateTime} - kolejność wywołania");
            if (_cascadeCounter == CascadeStorage._List.Count)
            {
                validateIntervals();
                Dispatcher.BeginInvoke(() =>
                {
                    dataGridCascadeRetention.ItemsSource = null;
                });
            }
        }
        #endregion
        #region private UIMethods
        /// <summary>
        /// UI Methods for inputs, executing work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timeScheduleApply_Button(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CascadeStorage == null)
                {
                    inputDateTime.Hour = short.Parse(txtBoxHours.Text);
                    inputDateTime.Minute = short.Parse(txtBoxMinutes.Text);

                    if (!Enumerable.Range(0, 23).Contains(inputDateTime.Hour) || !Enumerable.Range(0, 59).Contains(inputDateTime.Minute))
                    {
                        throw new ArgumentException();
                    }

                    var checkMidnightInputHour = inputDateTime.Hour == 0 ? inputDateTime.Hour = 22 : inputDateTime.Hour += regionDate; // need to refactor this declaration
                    inputDateTime.Second = (short)DateTime.Now.Second;
                }
                if (txtBoxScheduleCustomTime.IsEnabled && txtBoxScheduleCustomTime.Text != string.Empty)
                {
                    if (Convert.ToInt32(txtBoxScheduleCustomTime.Text) <= 0)
                    {
                        throw new FormatException();
                    }

                    double interval = Convert.ToDouble(txtBoxScheduleCustomTime.Text) * defaultScheduleInterval;
                    _scheduleIntervalTimer = new Timer(interval);
                    _scheduleIntervalTimer.Elapsed += onTimedCustomScheduleEvent;
                    _scheduleIntervalTimer.AutoReset = true;
                    _scheduleIntervalTimer.Start();
                    buttonApplyTimeChange.IsEnabled = false;
                    txtBoxLogs.Text += Logger.InfoWindowLog($"Uruchomiono zmianę daty i godziny wedle własnego interwału - {txtBoxScheduleCustomTime.Text} minut");
                }
                else if ((bool)chkBoxDefaultRetentionTimeChange.IsChecked)
                {
                    DateTime time = new DateTime();
                    time = DateTime.Now;

                    double interval = (TimeParameterValidator.midnightTime - time).TotalMilliseconds;
                    _scheduleIntervalTimer = new Timer(interval);
                    _scheduleIntervalTimer.Elapsed += onTimedScheduleEvent;
                    _scheduleIntervalTimer.AutoReset = true;
                    _scheduleIntervalTimer.Start();
                    buttonApplyTimeChange.IsEnabled = false;
                    txtBoxLogs.Text += Logger.InfoWindowLog($"Uruchomiono zmianę daty i godziny o północy - Czas do zmiany daty i godziny - {interval.ToString()} milisekund");
                }
                else if (CascadeStorage != null && CascadeStorage._List.Count > 0)
                {
                    txtBoxScheduleCustomTime.Text = CascadeStorage.CascadeInterval.ToString();
                    CascadeStorage.CascadeInterval *= defaultScheduleInterval;
                    _scheduleIntervalTimer = new Timer(CascadeStorage.CascadeInterval);
                    _scheduleIntervalTimer.Elapsed += onCascadeScheduleEvent;
                    _scheduleIntervalTimer.AutoReset = true;
                    _scheduleIntervalTimer.Start();
                    txtBoxLogs.Text += Logger.InfoWindowLog($"Uruchomiono zmianę daty i godziny wedle wywołania kaskadowego interwału - Lista zawiera {CascadeStorage._List.Count} zmian czasu.");
                }
                else
                {
                    DateTimeParameters.SetSystemTime(ref inputDateTime);
                    txtBoxLogs.Text += Logger.InfoWindowLog($"Zmieniono datę i godzinę na {inputDateTime}");
                }
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Pole godzin lub minut nie może byc puste", "Błąd");
            }
            catch (FormatException)
            {
                MessageBox.Show("Musisz wprowadzić liczby w zakresie od 0 do 23 dla godzin i od 0 do 59 dla minut. Dla własnego interwału wprowadź zakres minut od 1 do x ", "Błąd");
                clearTimeInputs();
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Musisz wprowadzić liczby w zakresie od 0 do 23 dla godzin i od 0 do 59 dla minut.", "Błąd");
                clearTimeInputs();
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message,"Krytyczny błąd");
                Logger.DebugFileLog(error.Message);
                clearTimeInputs();
            }
        }
        private void generatorRetentionApply_Button(object sender, RoutedEventArgs e)
        {
            try
            {
                buttonApplyGenerator.IsEnabled = false;
                txtBoxLogs.Text += Logger.InfoWindowLog($"Rozpoczynam generowanie {txtBoxFileAmount.Text} plików o rozmiarze od {txtBoxMaxFileSize.Text} do {txtBoxMaxFileSize.Text} {_fileType.ToUpper()}, do katalogu docelowego {txtBoxFilePath.Text}");
                string[] args = new string[] { "p", txtBoxFilePath.Text, "min", txtBoxMinFileSize.Text, "max", txtBoxMaxFileSize.Text, "u", _fileType, "c", txtBoxFileAmount.Text, "dc", "0" };
                Program.Main(args);

                if (txtBoxGeneratorInterval.IsEnabled)
                {
                    double interval = Convert.ToDouble(txtBoxGeneratorInterval.Text) * defaultScheduleInterval;
                    _generatorIntervalTimer = new System.Timers.Timer(interval);
                    _generatorIntervalTimer.Elapsed += onTimedGeneratorEvent;
                    _generatorIntervalTimer.AutoReset = true;
                    _generatorIntervalTimer.Start();
                    txtBoxLogs.Text += Logger.InfoWindowLog($"Wygenerowano pliki, i uruchomiono interwał wynoszący {txtBoxGeneratorInterval.Text} minut. Proszę Czekać!");
                }
                else
                {
                    txtBoxLogs.Text += Logger.InfoWindowLog("Wygnerowano pliki!");
                    buttonApplyGenerator.IsEnabled = true;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                Logger.DebugFileLog(err.Message);
            }
        }
        private void cascadeWindowOpen_Click(object sender, RoutedEventArgs e)
        {
            AddCascadeRetentionWindow window = new AddCascadeRetentionWindow();
            CascadeStorage = new DateTimeCascadeStorage();
            window.Show();
        }
        private void help_Button(object sender, RoutedEventArgs e)
        {
            HelpWindow window = new HelpWindow();
            window.Show();
        }
        private void timeScheduleRemove_Button(object sender, RoutedEventArgs e)
        {
            validateIntervals();
            clearTimeInputs();
        }
        private void generatorRetentionRemove_Button(object sender, RoutedEventArgs e)
        {
            buttonApplyGenerator.IsEnabled = true;
            validateIntervals();
        }
        private void close_Button(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void chkBoxTimeChangeActive_Checked(object sender, RoutedEventArgs e)
        {
            chkBoxDefaultRetentionTimeChange.IsChecked = true;
        }
        private void chkBoxTimeChangeActive_Unchecked(object sender, RoutedEventArgs e)
        {
            validateIntervals();
        }
        private void generatorCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            txtBoxGeneratorInterval.IsEnabled = true;
        }
        private void generatorCheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            txtBoxGeneratorInterval.IsEnabled = false;
            txtBoxGeneratorInterval.Text = string.Empty;
            validateIntervals();
        }
        private void chkBoxCustomScheduleTimeActive_Checked(object sender, RoutedEventArgs e)
        {
            txtBoxScheduleCustomTime.IsEnabled = true;
        }
        private void chkBoxCustomScheduleTimeActive_Unchecked(object sender, RoutedEventArgs e)
        {
            txtBoxScheduleCustomTime.Text = string.Empty;
            txtBoxScheduleCustomTime.IsEnabled = false;
            validateIntervals();
        }
        private void cbxFileSizeType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbxFileSizeType.SelectedIndex)
            {
                case 0:
                    _fileType = FileSizeType.b.ToString();
                    break;
                case 1:
                    _fileType = FileSizeType.kb.ToString();
                    break;
                case 2:
                    _fileType = FileSizeType.mb.ToString();
                    break;
                case 3:
                    _fileType = FileSizeType.gb.ToString();
                    break;
            }
        }
        #endregion
        #region private methods
        /// <summary>
        /// private validators of UI / fields.
        /// </summary>
        private void validateIntervals()
        {
            if (_generatorIntervalTimer != null)
            {
                _generatorIntervalTimer.Stop();
                _generatorIntervalTimer.Dispose();
            }
            if (_scheduleIntervalTimer != null)
            {
                _scheduleIntervalTimer.Stop();
                _scheduleIntervalTimer.Dispose();
            }
            txtBoxLogs.Text += Logger.InfoWindowLog("Wyczyszczono i wyłączono interwały generatora plików i zmiany czasu i daty");
        }
        private void clearTimeInputs()
        {
            txtBoxHours.Text = string.Empty;
            txtBoxMinutes.Text = string.Empty;
            txtBoxScheduleCustomTime.Text = string.Empty;
            txtBoxLogs.Text += Logger.InfoWindowLog("Wyczyszczono i wyłączono dane dotyczące czasu i jego interwału.");
        }
        public void ValidateCascadeRetention()
        {
            dataGridCascadeRetention.ItemsSource = null;
            CascadeStorage._List.Clear();
            txtBoxLogs.Text += Logger.InfoWindowLog("Wyczyszczono listę wywołania kaskadowego.");
        }
        #endregion
    }
}
